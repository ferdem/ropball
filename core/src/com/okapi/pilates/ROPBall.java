package com.okapi.pilates;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class ROPBall implements ApplicationListener {

    public static Screen screen;
    public static SpriteBatch batch;

    public static int screenWidth;
    public static int screenHeight;

    public static void setScreen (Screens screen) {
        if (ROPBall.screen != null) screen.hide();
        ROPBall.screen = screen;
        if (ROPBall.screen != null) screen.show();
    }

    @Override
    public void create() {
        screenWidth = Gdx.graphics.getWidth();
        screenHeight = Gdx.graphics.getHeight();
        batch = new SpriteBatch();

        setScreen(Screens.MENU);
    }

    @Override
    public void dispose () {
        if (screen != null) screen.hide();
    }

    @Override
    public void pause () {
        if (screen != null) screen.pause();
    }

    @Override
    public void resume () {
        if (screen != null) screen.resume();
    }

    @Override
    public void render () {
        if (screen != null) screen.render(Gdx.graphics.getDeltaTime());
    }

    @Override
    public void resize (int width, int height) {
        if (screen != null) screen.resize(width, height);
    }

}
