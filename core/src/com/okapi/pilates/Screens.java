package com.okapi.pilates;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.okapi.pilates.object.BlackBall;
import com.okapi.pilates.object.PlatesBall;

import java.util.ArrayList;
import java.util.HashSet;

import static com.okapi.pilates.ROPBall.batch;
import static com.okapi.pilates.ROPBall.screenHeight;
import static com.okapi.pilates.ROPBall.screenWidth;
import static com.okapi.pilates.ROPBall.setScreen;

// recommended links ~ http://javarevisited.blogspot.com.tr/2011/08/enum-in-java-example-tutorial.html
public enum Screens implements Screen, InputProcessor {

    SURVIVAL {

        private PlatesBall platesBall;
        private OrthographicCamera camera;
        private ArrayList<BlackBall> blackBalls;
        private HashSet<BlackBall> crashedBalls;

        @Override
        void init() {
            camera = new OrthographicCamera();
            camera.setToOrtho(false, screenWidth, screenHeight);

            platesBall = new PlatesBall(screenWidth / 2, screenHeight / 2);
            blackBalls = new ArrayList<BlackBall>();
            crashedBalls = new HashSet<BlackBall>();

            spawnBlackBall(3);
        }

        @Override
        public void show() {
            Gdx.input.setCatchBackKey(true);
            Gdx.input.setInputProcessor(this);
        }

        @Override
        public void render(float delta) {
            Gdx.gl.glClearColor(1, 1, 1, 0);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

            batch.begin();
            for (BlackBall blackBall : blackBalls)
                blackBall.draw();
            platesBall.draw();
            batch.end();

            update();
            collisionDetection();
        }

        @Override
        public boolean keyDown(int keycode) {
            if (keycode == Input.Keys.BACK)
                setScreen(MENU);
            return false;
        }

        private void update() {
            camera.update();
            batch.setProjectionMatrix(camera.combined);
            platesBall.update();

            for (BlackBall blackBall : blackBalls)
                blackBall.follow(platesBall);
        }

        private void collisionDetection() {
            for (int i = 0; i < blackBalls.size(); i++) {
                if (platesBall.catchedBy(blackBalls.get(i))) {
                    init(); // tmp
                }
                for (int j = i + 1; j < blackBalls.size(); j++) {
                    BlackBall ithBall = blackBalls.get(i);
                    BlackBall jthBall = blackBalls.get(j);

                    if (ithBall.overlaps(jthBall)) {
                        crashedBalls.add(ithBall);
                        crashedBalls.add(jthBall);
                    }
                }
            }

            if (crashedBalls.size() != 0) {
                spawnBlackBall(crashedBalls.size());
                blackBalls.removeAll(crashedBalls);
                crashedBalls.clear();
            }
        }

        private void spawnBlackBall() {
            int x = MathUtils.random(16, screenWidth - 16);
            int y = MathUtils.random(16, screenHeight - 16);
            float velocity = MathUtils.random(200, 400);
            blackBalls.add(new BlackBall(x, y, velocity));
        }

        private void spawnBlackBall(int many) {
            for (int i = 0; i < many; i++)
                spawnBlackBall();
        }

    },

    MENU {

        private Stage stage;
        private Skin skin;

        @Override
        void init() {
            stage = new Stage();
            skin = new Skin(Gdx.files.internal("ui/menuSkin.json"), new TextureAtlas("ui/atlas.pack"));
            Table table = new Table(skin);
            table.setFillParent(true);

            Label heading = new Label("ROPBall", skin, "big");
            heading.setFontScale(2);
            heading.setColor(Color.BLACK);

            TextButton buttonPlay = new TextButton("PLAY", skin, "big");
            buttonPlay.addListener(new ClickListener() {

                @Override
                public void clicked(InputEvent event, float x, float y) {
                    setScreen(SURVIVAL);
                }
            });

            TextButton buttonSettings = new TextButton("SETTINGS", skin);
            buttonSettings.addListener(new ClickListener() {

                @Override
                public void clicked(InputEvent event, float x, float y) {
                    System.out.println("Settings");
                }
            });

            TextButton buttonExit = new TextButton("EXIT", skin, "big");
            buttonExit.addListener(new ClickListener() {

                @Override
                public void clicked(InputEvent event, float x, float y) {
                    Gdx.app.exit();
                }
            });

            buttonPlay.pad(20);
            buttonSettings.pad(20);
            buttonExit.pad(20);

            table.add(heading).spaceBottom(100).row();
            table.add(buttonPlay).spaceBottom(20).row();
            table.add(buttonSettings).spaceBottom(20).row();
            table.add(buttonExit);

            stage.addActor(table);
        }

        @Override
        public void show() {
            Gdx.input.setCatchBackKey(true);
            Gdx.input.setInputProcessor(stage);
        }

        @Override
        public void render(float delta) {
            Gdx.gl.glClearColor(1, 1, 1, 0);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

            stage.act(delta);
            stage.draw();
        }

    };

    Screens() {
        init();
    }

    abstract void init();

    // Screen interface ~ unimplemented methods...
    public void resize(int width, int height) {}
    public void pause() {}
    public void resume() {}
    public void hide() {}
    public void dispose() {}

    // InputProcessor interface ~ unimplemented methods...
    public boolean keyDown(int keycode) { return false; }
    public boolean keyUp(int keycode) { return false; }
    public boolean keyTyped(char character) { return false; }
    public boolean touchDown(int screenX, int screenY, int pointer, int button) { return false; }
    public boolean touchUp(int screenX, int screenY, int pointer, int button) { return false; }
    public boolean touchDragged(int screenX, int screenY, int pointer) { return false; }
    public boolean mouseMoved(int screenX, int screenY) { return false; }
    public boolean scrolled(int amount) { return false; }

}
