package com.okapi.pilates;

import com.okapi.pilates.object.GameObject;

public enum Skills {

    SLOWER("slower.png") {
        @Override
        public void enable() {

        }

        @Override
        public void disable() {

        }
    };

    public GameObject object;

    Skills(String path) {
        object = new GameObject(0, 0, 32, path);
    }

    abstract void enable();
    abstract void disable();

}
