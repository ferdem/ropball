package com.okapi.pilates.object;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Circle;

import static com.okapi.pilates.ROPBall.batch;

public class GameObject {

    protected Circle shape;
    protected Sprite sprite;

    public GameObject(int x, int y, int radius, String path) {
        shape = new Circle(x, y, radius);
        sprite = new Sprite(new Texture(path));
        sprite.setCenter(x, y);
    }

    public boolean overlaps(GameObject gameObject) {
        return shape.overlaps(gameObject.shape);
    }

    public void draw() {
        sprite.draw(batch);
    }

}