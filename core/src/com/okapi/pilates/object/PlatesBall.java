package com.okapi.pilates.object;

import com.badlogic.gdx.Gdx;

import static com.okapi.pilates.ROPBall.batch;
import static com.okapi.pilates.ROPBall.screenHeight;
import static com.okapi.pilates.ROPBall.screenWidth;


public class PlatesBall extends GameObject {

    private static final float MOVE_VELOCITY = 500;
    private static final float BOUNCE_VELOCITY = 50;

    public boolean rising;

    public PlatesBall(int x, int y) {
        super(x, y, 64, "pilatesBall.png");
    }

    @Override
    public void draw() {
        super.draw();

        // when the ball is on the edge of the screen
        if (shape.x - shape.radius <= 0) // left edge
            sprite.setCenterX(shape.x + screenWidth);
        else if (shape.x + shape.radius >= screenWidth) // right edge
            sprite.setCenterX(shape.x - screenWidth);
        sprite.draw(batch);

        if (shape.y - shape.radius <= 0) // down edge
            sprite.setCenterY(shape.y + screenHeight);
        else if (shape.y + shape.radius >= screenHeight) // up edge
            sprite.setCenterY(shape.y - screenHeight);
        sprite.draw(batch);

        teleportBall();
    }

    private void teleportBall() {
        // teleport the ball
        if (shape.x < -sprite.getWidth())
            shape.setX(shape.x + screenWidth);
        else if (shape.x > screenWidth)
            shape.setX(shape.x - screenWidth);
        else if (shape.y < -sprite.getHeight())
            shape.setY(shape.y + screenHeight);
        else if(shape.y > screenHeight)
            shape.setY(shape.y - screenHeight);
    }

    private void moveBall() {
        float accelX = Gdx.input.getAccelerometerX();
        float accelY = Gdx.input.getAccelerometerY();

        shape.x += (accelY / 9.81f) * MOVE_VELOCITY * Gdx.graphics.getDeltaTime();
        shape.y += -(accelX / 9.81f) * MOVE_VELOCITY * Gdx.graphics.getDeltaTime();

        sprite.setCenter(shape.x, shape.y);
    }

    private void bounceBall() {
        if (shape.radius > 64)
            rising = false;
        else if (shape.radius < 32)
            rising = true;

        if (rising)
            shape.radius += BOUNCE_VELOCITY * Gdx.graphics.getDeltaTime();
        else
            shape.radius -= BOUNCE_VELOCITY * Gdx.graphics.getDeltaTime();

        sprite.setSize(2 * shape.radius, 2 * shape.radius);
    }

    public boolean catchedBy(BlackBall blackBall) {
        return shape.radius < 36 && overlaps(blackBall);
    }

    public void update() {
        moveBall();
        bounceBall();
    }

}