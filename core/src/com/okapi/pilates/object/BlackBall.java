package com.okapi.pilates.object;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;

public class BlackBall extends com.okapi.pilates.object.GameObject {

    private float velocity;

    public BlackBall(int x, int y, float velocity) {
        super(x, y, 32, "blackBall.png");
        this.velocity = velocity;
    }

    public void follow(com.okapi.pilates.object.GameObject target) {
        float followX = target.shape.x - shape.x;
        float followY = target.shape.y - shape.y;
        float lenSqr = followX * followX + followY * followY;

        followX *= Math.sqrt(1 / lenSqr);
        followY *= Math.sqrt(1 / lenSqr);

        shape.x += followX * velocity * Gdx.graphics.getDeltaTime();
        shape.y += followY * velocity * Gdx.graphics.getDeltaTime();

        sprite.setCenter(shape.x, shape.y);
    }
}